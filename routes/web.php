<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//primera ruta de get
Route::get('hola', function(){
echo "Hola estoy en  laravel";
});

Route::get('arreglo', function(){

//Crear arreglo estudiantes

	$estudiantes = [ "AD" =>"andres",
	                 "LAN" => "laura" ,
	                 "ROG" => "rodrigo" ];
	//var_dump($estudiantes);
foreach($estudiantes as $indice => $estudiante ){
	echo "$estudiante tiene indice $indice <hr />";
}
});

Route::get('paises', function(){
//crear arreglo de paises
$paises = [

	"Colombia" => [ 
	"Capital" => "Bogota",
	"Moneda" => "Peso" ,
	"Plato" => "huevo con leche" ],

	"Brasil" => [ 
	"Capital" => "Brasilia",
	"Moneda" => "Real Brasileño" ,
	"Plato" => "vatapa" ], 

	"Ecuador" => [ 
	"Capital" => "Quito",
	"Moneda" => "Dolar" ,
	"Plato" => "vatapa" ], 

	"Peru" => [ 
	"Capital" => "Quito",
	"Moneda" => "Dolar" ,
	"Plato" => "Pollo a la brasa" ]
 ];
 return view ('paises')->whith( "paises", $paises);
   /* foreach($paises as $pais => $infopais){
	echo "<h2> $pais </h2>";
    echo "Capital:". $infopais["Capital"]. "<br />";
	echo "Moneda:". $infopais["Moneda"]. "<br />";
	echo "Plato:". $infopais["Plato"];
	echo "<hr />"; }*/
	
});